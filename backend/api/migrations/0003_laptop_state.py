# Generated by Django 3.0.4 on 2020-03-29 16:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20200329_1217'),
    ]

    operations = [
        migrations.AddField(
            model_name='laptop',
            name='state',
            field=models.CharField(choices=[('INVENTORY', 'INVENTORY '), ('IMAGING', 'IMAGING'), ('TRANSIT', 'TRANSIT'), ('HUB', 'HUB'), ('PICKEDUP', 'PICKEDUP')], default='INVENTORY', max_length=10),
        ),
    ]
