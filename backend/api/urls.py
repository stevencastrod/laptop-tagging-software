from django.urls import path
from . import views

urlpatterns = [
    path('laptops/', views.laptops, name='laptops'),
    path('laptops/<int:TagNumber>', views.laptop_detail)
]
