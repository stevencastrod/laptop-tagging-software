from django.db import models

# Create your models here.


class laptop(models.Model):
    INVENTORY = 'INVENTORY'
    IMAGING = 'IMAGING'
    TRANSIT = 'TRANSIT'
    HUB = 'HUB'
    PICKEDUP = 'PICKEDUP'
    STATE_CHOICES = [
        (INVENTORY, 'INVENTORY '),
        (IMAGING, 'IMAGING'),
        (TRANSIT, 'TRANSIT'),
        (HUB, 'HUB'),
        (PICKEDUP, 'PICKEDUP'),
    ]
    TagNumber = models.IntegerField(null=False, unique=True)
    SerialNumber = models.CharField(max_length=50, null=False, unique=True)
    MacAddress = models.CharField(max_length=50, null=True, blank=True)
    Cwid = models.CharField(max_length=50, null=True, blank=True)
    state = models.CharField(
        max_length=10, choices=STATE_CHOICES, default=INVENTORY)

    class Meta:
        verbose_name = ("laptop")
        verbose_name_plural = ("laptops")

    def __str__(self):
        return str(self.TagNumber)

    def get_absolute_url(self):
        return reverse("laptop_detail", kwargs={"TagNumber": self.TagNumber})
