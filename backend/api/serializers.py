from rest_framework import serializers
from .models import laptop


class laptopSerializer(serializers.ModelSerializer):
    class Meta:
        model = laptop
        fields = '__all__'
