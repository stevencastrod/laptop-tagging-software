from .serializers import laptopSerializer
from django.http import HttpResponse, JsonResponse
from .models import laptop
from rest_framework.parsers import JSONParser
from django.views.decorators.csrf import csrf_exempt

# Create your views here.


@csrf_exempt
def laptops(request):
    """
    Retrieve all laptops, or add one
    """
    if request.method == 'GET':
        laptops = laptop.objects.all()
        serializer = laptopSerializer(laptops, many=True)
        return JsonResponse(serializer.data, safe=False)
        
    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = laptopSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


@csrf_exempt
def laptop_detail(request, TagNumber):
    """
    Retrieve, update or delete a single laptop.
    """
    try:
        Laptop = laptop.objects.get(TagNumber=TagNumber)
    except laptop.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = laptopSerializer(Laptop)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = laptopSerializer(Laptop, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        Laptop.delete()
        return HttpResponse(status=204)
