import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
Vue.use(Vuex);

const uri = "http://localhost:8000/laptops/";

export default new Vuex.Store({
  state: {
    laptops: [],
    error: ""
  },
  mutations: {
    updateLaptops: (state, laptops) => (state.laptops = laptops),
    newLaptop: (state, response) => state.laptops.unshift(response),
    updateError: (state, err) => (state.error = err)
  },
  actions: {
    async getLaptops({ commit }) {
      const laptops = await axios
        .get(uri)
        .catch(err => commit("updateError", err));
      commit("updateLaptops", laptops.data);
    },
    async addLaptop({ commit }, laptop) {
      const response = await axios
        .post(uri, laptop)
        .catch(err => commit("updateError", err));
      commit("newLaptop", response.data);
    },
    async updateLaptop({ commit }, laptop) {
      const response = await axios
        .put(`${uri}${laptop.TagNumber}`, laptop)
        .catch(err => commit("updateError", err));
      commit("updateLaptops", response.data);
    }
  },
  getters: {
    laptops: state => {
      return state.laptops;
    },
    numberOflaptopsInState: state => stage => {
      return state.laptops.filter(laptopState => {
        return laptopState.state == stage;
      }).length;
    }
  },
  modules: {}
});
